package main

import (
	"fmt"
	"log"
	"net/http"
	"os"

	"github.com/joho/godotenv"
)

func indexHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Nanana")
	fmt.Fprintln(w, "Welcome!")
}

func aboutHandler(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, "This is who I am:")
}

func main() {
	err := godotenv.Load()
	if err != nil {
		log.Fatal("Could not load the env file")
	}
	http.HandleFunc("/", indexHandler)
	http.HandleFunc("/about", aboutHandler)
	http.ListenAndServe(fmt.Sprintf(":%s", os.Getenv("APP_PORT")), nil)
}
