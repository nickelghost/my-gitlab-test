FROM golang:1.13-alpine

RUN apk update && apk add --no-cache build-base git wait4ports

RUN go get github.com/canthefason/go-watcher && \
    go install github.com/canthefason/go-watcher/cmd/watcher

RUN mkdir /app

WORKDIR /app

COPY . .

RUN go build -o bin/mygitlabtest

EXPOSE 8080

CMD ["/app/bin/mygitlabtest"]
